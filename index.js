const web = require ('webbox');
const readline = require('readline');
const fs = require ('fs');

var platform = new web.Server.Box();

platform.createServer(1337);
platform.setServerRoot('/www');

var vd = new web.VirtualDrive.Box(platform, 'Server/');

var properties = {
    maxSize: 5*1024*1024,
    fileTypes: ['text/plain'],
    maxNameLength: 4
};

vd.onUpload('/upload', '/', properties, function(req, res, result){
    platform.sendJSON(res, result);
});

platform.onPost('/onGetCsv', function (req, res) {
    var result = "";
    var error = false;
    var r1 = readline.createInterface({
        input: fs.createReadStream('./www/Server/' + req.body.filename)
    });

    r1.on('line', function (line) {
        console.log('Line from file:', line);
        result += line + ',2,' + parseInt(line) * 2 + '\r\n';
        if (isNaN(parseInt(line))){error=true;}
    }).on('close', function () {
        var filename = req.body.filename.substring(0,4);
        if (error === false) {
            fs.writeFile('./www/Server/' + filename + '.csv', result, function (err) {
                if (err) throw err;
                console.log('It\'s saved!');
                platform.sendJSON(res, {status: 1, filename: filename});
            });
        }
        else {
            fs.unlink('./www/Server/' + filename + '.txt');
            platform.sendJSON(res, {status: 513});
        }
    });
});

platform.onGet('/download', function (req, res) {
   res.download('./www/Server/'+req.query.filename+'.csv');
});

platform.onPost('/onGetTable', function (req, res) {
    var result = [];
    var error = false;
    var r1 = readline.createInterface({
        input: fs.createReadStream('./www/Server/' + req.body.filename)
    });

    r1.on('line', function (line) {
        console.log('Line from file:', line);
        result.push(parseInt(line) * 2);
        if (isNaN(parseInt(line))){error=true;}
    }).on('close', function () {
        if (error === false) {
            platform.sendJSON(res, {status: 1, result: result});
        }
        else {
            fs.unlink('./www/Server/' + req.body.filename);
            platform.sendJSON(res, {status: 513});
        }
    });
});


