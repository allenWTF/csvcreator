var filename;
var file;

document.getElementById("uploadButton").onchange = function() {
    file=this.files[0];
    if ((file.size / 1000000) > 2) {
        alert("Файл должен весить не более 2 МБ!");
        return;
    }
    if (file.type !== 'text/plain')
    {
        alert("Неверный тип файла. Требуется .txt!");
        return;
    }
    if (file.size === 0) {
        alert("Не могу обработать пустой файл. Попробуйте еще раз.");
        return;
    }
    if (file) {
        send(file);
    }

};

function addEventHandler(obj, evt, handler) {
    if (obj.addEventListener) {
        obj.addEventListener(evt, handler, false);
    } else if (obj.attachEvent) {
        obj.attachEvent('on' + evt, handler);
    } else {
        obj['on' + evt] = handler;
    }
}

function reset() {
    filename = null;
    file = null;
    document.getElementById("upButton").style.display = "block";
    document.getElementById("or").style.display = "block";
    document.getElementById("reset").style.display = "none";
    document.getElementById("table").innerHTML="";
    document.getElementById("uploadButton").value = "";
    log("Положите ваш файл сюда");
}

if (window.FileReader) {
    addEventHandler(window, 'load', function () {
        var status = document.getElementById('status');
        var drop = document.getElementById('drop');
        var list = document.getElementById('list');

        function cancel(e) {
            if (e.preventDefault) {
                e.preventDefault();
            }
            return false;
        }

        addEventHandler(drop, 'dragover', cancel);
        addEventHandler(drop, 'dragenter', cancel);
        addEventHandler(drop, 'drop', function (e) {
            e = e || window.event;
            if (e.preventDefault) {
                e.preventDefault();
            }

            file = e.dataTransfer.files[0];
            if ((file.size / 1000000) > 2) {
                alert("Файл должен весить не более 2 МБ!");
                return;
            }
            if (file.type !== 'text/plain')
            {
                alert("Неверный тип файла. Требуется .txt!");
                return;
            }
            if (file.size === 0) {
                alert("Не могу обработать пустой файл. Попробуйте еще раз.");
                return;
            }
            send(file);
            return false;
        });
    });
} else {
    document.getElementById('status').innerHTML = 'Your browser does not support the HTML5 FileReader.';
}


function log(text) {
    document.getElementById("droptext").innerHTML = text;
}

function send(file) {
    var xhr = new XMLHttpRequest();

    xhr.upload.onprogress = function (event) {
        log(event.loaded + ' / ' + event.total);
    };

    xhr.onload = xhr.onerror = function () {
        var response = JSON.parse(xhr.responseText);
        if (response.files[0].saved === false) {
            alert("Неверный тип файла. Требуется .txt!");
            reset();
            return;
        }
        else if (this.status == 200) {
            log("Файл успешно загружен!");
            document.getElementById("upButton").style.display = "none";
            document.getElementById("reset").style.display = "block";
            document.getElementById("or").style.display="none";
            filename = response.files[0].name;
        } else {
            log("error " + this.status);
        }
    };

    xhr.open("POST", "upload", true);
    var formData = new FormData();
    formData.append("file", file);
    xhr.send(formData);
}

function getCsv() {
    if (!filename) {
        alert("Сначала загрузите файл!");
        return;
    }

    var json = {
        filename: filename
    };

    sendRequest(json,"onGetCsv", onGotCsv);
}

function onGotCsv(data) {
    var response = JSON.parse(data);
    downloadCsv(response);
}

function downloadCsv(response) {
    if (response.status === 1) {
        window.location = window.location + 'download?filename=' + response.filename;
    }
    else if (response.status === 513) {
        alert("Неправильный файл. В файле могут находится только числа, записанные с новой строки.");
        reset();
    }
}

function getTable() {
    if (!filename) {
        alert("Сначала загрузите файл!");
        return;
    }
    var json = {
        filename: filename
    };
    sendRequest(json,"onGetTable",onGotTable);
}

function onGotTable(data) {
    var response = JSON.parse(data);
    if (response.status === 1) {
        document.getElementById('table').innerHTML = '<tr><th>Число</th><th>Множитель</th><th>Результат</th></tr>';
        response.result.forEach(function (item) {
            var cell1 = item / 2;
            var cell2 = 2;
            var cell3 = item;
            document.getElementById('table').innerHTML +=
                '<tr>' +
                '<td>' + cell1 + '</td>' +
                '<td>' + cell2 + '</td>' +
                '<td>' + cell3 + '</td>' +
                '</tr>';
        });
    }
    else if (response.status === 513) {
        alert("Неправильный файл. В файле могут находится только числа, записанные с новой строки.");
        reset();
    }
}

function sendRequest(data,url,callback) {
    var xhr = new XMLHttpRequest();

    xhr.onload = function () {
        callback(xhr.responseText);
    };
    xhr.open("POST", url, true);
    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhr.send(JSON.stringify(data));
}
