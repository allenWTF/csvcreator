# README #

Тестовое задание для Globus. Артем Скачков.

### Что это? ###

Приложение, выполняющее все задачи и усложнения из данного [задания](https://docs.google.com/document/d/1OZUF4BHdYl2vtM7XxDH_gsNjBcjWUvrJiZVMEyJEpRQ/edit).

### С помошью чего сделано? ###

Клиентская часть:

* HTML5
* CSS
* JS

Серверная часть:

* Node.js
* Npm модули: webbox (принимал участие в разработке), multiparty и express

### Как попробовать? ###

Клонировать всё из данного репозитория в любую папку. Находясь в этой папке, установить необходимые npm модули с помощью команд:

```
npm install webbox
npm install multiparty
npm install express
```

Запустить сервер с помощью команды:

```
node index.js
```
Приложение запустится по адресу http://localhost:1337

### Примеры работы ###

![globus.gif](https://psv4.vk.me/c810424/u19592568/docs/4020e3a10386/globus.gif?extra=LRwoU-NcNlQiZJB49S7FK41Bm40bSnHdVPVnT3__T7vxeqR-nzNOC72NqPJ_qpdTlwIAKNAhoVoDZYmWN9zYcVujDYKV-0OVVoEfkxq6Jw2PBNEl2EVFlA)